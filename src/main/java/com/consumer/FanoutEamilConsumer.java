package com.consumer;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 *
 */
@Component
public class FanoutEamilConsumer {
    /**
     * 如果开启手动确认模式，则方法注释上必须这样写@RabbitListener(queues = "fanout_eamil_queue")，可以和短信消费者注解模式做个区分 FanoutSmsConsume.java
     * @param message
     * @param headers
     * @param channel
     * @throws Exception
     */
    @RabbitListener(queues = "fanout_eamil_queue")
    public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws Exception {
        System.out.println(Thread.currentThread().getName() + ",邮件消费者获取生产者消息msg:" + new String(message.getBody(), "UTF-8")
                + ",messageId:" + message.getMessageProperties().getMessageId());

        //String messageId = message.getMessageProperties().getMessageId();
        String msg = new String(message.getBody(), "UTF-8");
        System.out.println("邮件消费者获取生产者消息msg:" + msg );
        JSONObject jsonObject = JSONObject.parseObject(msg);
        Integer timestamp = jsonObject.getInteger("timestamp");
        try {
            int result = 1 / timestamp;
            // 通知mq服务器删除该消息，如果这里不手动确认的话，该消息会一直存在不被消费
            /**
             * deliveryTag:该消息的index
             multiple：是否批量.true:将一次性ack所有小于deliveryTag的消息。
             */
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * deliveryTag:该消息的index
             multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
             requeue：被拒绝的是否重新入队列
             */
            //如果这里不捕获异常，并且拒收该消息的话，该消息是不会被加入到死信队列的
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
        }

        /**
         * deliveryTag:该消息的index
         multiple：是否批量.true:将一次性ack所有小于deliveryTag的消息。
         */
      /*  Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
        channel.basicAck(deliveryTag, false);//如果这里不手动确认的话，该消息会一直存在不被消费*/

        /**
         * deliveryTag:该消息的index
           multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
           requeue：被拒绝的是否重新入队列
         */
        //channel.basicNack(deliveryTag, false, true);
    }
}