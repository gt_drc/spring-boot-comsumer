package com.consumer;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @RabbitListener 底层通过aop实现拦截，如果程序没有报错,会自动实现补偿机制
 * 补偿（重试）机制：队列服务器发送补偿请求（会一直做重试，直到不抛出异常，除非 消费端配置重试策略）
 * 如果消费者程序出现异常，那消息能消费成功吗？（不能）
 * 重新配置重试策略
 */
@Component
@RabbitListener(queues = "fanout_sms_queue")
public class FanoutSmsConsumer {
   @RabbitHandler
    public void process(String msg) throws InterruptedException {
        System.out.println("短信消费者获取生产者消息msg:" + msg);

        /**
         * 这里有个疑问：
         * 在重试机制里我设置了3秒重试一次，在这里设置程序休眠5秒，也就是程序还没执行完成，难道下次重试就来了？
         * 答：不会的。原因有两点：
         * 1、所谓的重试是指：程序执行完成后或者抛出异常后，3秒后开始重试
         * 2、重试机制是单线程
         */

        Thread.sleep(5000);
        System.out.println("休眠完成:");
        //如果程序出现异常，会自动实现补偿机制
        int i = 1/0;

    }

}
